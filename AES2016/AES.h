#pragma once
#include <iostream>

using namespace std;

int polyMul(int num1, int num2);
int exEuclid(int u);

int calculateSBOX(int num);
void makeSBOX();
int getSBOX(int num);
int getIBOX(int num);

int AES(int* plain, int* key);

