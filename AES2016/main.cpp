#include "AES.h"
#include <stdio.h>
#include <fstream>

int box[4][4];

int main()
{
	//bin 파일이므로 바이너리로 파일을 읽고 쓴다
	FILE * fPlain = fopen("plain.bin", "rb");
	FILE * fKey = fopen("key.bin", "rb"); 
	
	//open에러 확인
	if (fPlain == NULL || fKey == NULL) {
		puts("파일 오픈 실패");
		return 0;
	}
		
	int plain[16];
	int key[16];

	for (int i = 0; i < 16; i++)
	{
		plain[i] = fgetc(fPlain);
		key[i] = fgetc(fKey);
	}
	
	AES(plain, key);

	fclose(fPlain);
	fclose(fKey);
		
	
	return 0;
	
}