#include "AES.h"
#include <iostream>


using namespace std;

//라운드 횟수 정의
#define ROUND 10

//암호화에 사용할 배열들
int state[4][4];
int roundKey[240];

//key expansion함수
void KeyExpansion(int *key)
{
	//RC배열 생성
	int RC[10] = { 1 };
	for (int i = 1; i < 10; i++)
	{
			RC[i] = polyMul(RC[i - 1], 2);
	}


	//먼저 키를 복사한다(0 round)
	for (int i = 0; i < 4; i++)
	{
		roundKey[i * 4] = key[i * 4];
		roundKey[i * 4 + 1] = key[i * 4 + 1];
		roundKey[i * 4 + 2] = key[i * 4 + 2];
		roundKey[i * 4 + 3] = key[i * 4 + 3];
	}

	cout << "KEY EXPANSION\nROUND 0: ";
	for (int i = 0; i < 16; i++)
	{
		printf("%02X ", roundKey[i]);
	}
	cout << endl;

	//R-function을 위한 배열
	int R[4];

	//각 라운드키를 생성한다
	for (int r = 1; r < ROUND + 1; r++)
	{
		for (int i = 0; i < 4; i++)
		{
			//첫줄은 R function과 XOR수행
			if (i == 0)
			{
				R[0] = getSBOX(roundKey[16 * (r - 1) + 7]) ^ RC[r-1];
				R[1] = getSBOX(roundKey[16 * (r - 1) + 11]);
				R[2] = getSBOX(roundKey[16 * (r - 1) + 15]);
				R[3] = getSBOX(roundKey[16 * (r - 1) + 3]);

				roundKey[16 * r] = roundKey[16 * (r - 1)] ^ R[0];
				roundKey[16 * r + 4] = roundKey[16 * (r - 1) + 4] ^ R[1];
				roundKey[16 * r + 8] = roundKey[16 * (r - 1) + 8] ^ R[2];
				roundKey[16 * r + 12] = roundKey[16 * (r - 1) + 12] ^ R[3];
			}
			else //이외엔 XOR 수행
			{
				roundKey[16 * r + i] = roundKey[16 * (r - 1) + i] ^ roundKey[16 * r + i - 1];
				roundKey[16 * r + i + 4] = roundKey[16 * (r - 1) + i + 4] ^ roundKey[16 * (r - 1) + i + 3];
				roundKey[16 * r + i + 8] = roundKey[16 * (r - 1) + i + 8] ^ roundKey[16 * (r - 1) + i + 7];
				roundKey[16 * r + i + 12] = roundKey[16 * (r - 1) + i + 12] ^ roundKey[16 * (r - 1) + i + 11];
			}
		}

		//각 라운드의 결과 출력
		cout << "ROUND " << r << ": ";
		for (int i = 0; i < 16; i++)
		{
			printf("%02X ",roundKey[16 * r + i]);
		}
		cout << endl;
	}
}

//addroundKey 함수
void AddroundKey(int round)
{
	//input과 roundkey xor
	for (int i = 0; i<4; i++)
	{
		for (int j = 0; j<4; j++)
		{
			state[i][j] ^= roundKey[round  * 16 + i * 4 +  j];
		}
	}
	
	//AR결과 출력
	cout << "AR: ";
	for (int i = 0; i < 4; i++)
	{
		for(int j=0; j<4; j++)
			printf("%02X ", state[i][j]);
	}
	cout << endl;
}


//Substitutes bytes 함수
void SubBytes()
{
	//각 칸에 SubByte 수행
	for (int i = 0; i<4; i++)
	{
		for (int j = 0; j<4; j++)
		{
			state[i][j] = getSBOX(state[i][j]);

		}
	}

	//SB결과 출력
	cout << "SB: ";
	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j<4; j++)
			printf("%02X ", state[i][j]);
	}
	cout << endl;
}

//inverse Substitutes bytes 함수
void invSubBytes()
{
	//inverse SBOX 이용
	for (int i = 0; i<4; i++)
	{
		for (int j = 0; j<4; j++)
		{
			state[i][j] = getIBOX(state[i][j]);

		}
	}

	//SB결과 출력
	cout << "SB: ";
	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j<4; j++)
			printf("%02X ", state[i][j]);
	}
	cout << endl;
}

//ShiftRow함수
void ShiftRows()
{
	int temp;

	//좌로 한칸씩 민다
	temp = state[1][0];
	state[1][0] = state[1][1];
	state[1][1] = state[1][2];
	state[1][2] = state[1][3];
	state[1][3] = temp;

	//좌로 두칸씩 민다
	temp = state[2][0];
	state[2][0] = state[2][2];
	state[2][2] = temp;

	temp = state[2][1];
	state[2][1] = state[2][3];
	state[2][3] = temp;

	//좌로 세칸씩 이동(우로 한칸)
	temp = state[3][0];
	state[3][0] = state[3][3];
	state[3][3] = state[3][2];
	state[3][2] = state[3][1];
	state[3][1] = temp;

	//SR결과 출력
	cout << "SR: ";
	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j<4; j++)
			printf("%02X ", state[i][j]);
	}
	cout << endl;
}

//inverse ShiftRow함수
void invShiftRows()
{
	int temp;

	//우로 한칸씩 민다
	temp = state[1][0];
	state[1][0] = state[1][3];
	state[1][3] = state[1][2];
	state[1][2] = state[1][1];
	state[1][1] = temp;

	//두칸씩 민다
	temp = state[2][0];
	state[2][0] = state[2][2];
	state[2][2] = temp;

	temp = state[2][1];
	state[2][1] = state[2][3];
	state[2][3] = temp;

	//우로 세칸씩 이동(좌로 한칸)
	temp = state[3][0];
	state[3][0] = state[3][1];
	state[3][1] = state[3][2];
	state[3][2] = state[3][3];
	state[3][3] = temp;

	//SR결과 출력
	cout << "SR: ";
	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j<4; j++)
			printf("%02X ", state[i][j]);
	}
	cout << endl;
}

//MixColumn함수
void MixColumns()
{
	int tmp[4];
	for (int i = 0; i<4; i++)
	{
		//계산을 위해 tmp에 기존값을 넣는다
		tmp[0] = state[0][i];
		tmp[1] = state[1][i];
		tmp[2] = state[2][i];
		tmp[3] = state[3][i];
		
		//행렬 계산 수행
		state[0][i] = polyMul(2, tmp[0]) ^ polyMul(3, tmp[1]) ^ tmp[2] ^ tmp[3];
		state[1][i] = tmp[0] ^ polyMul(2, tmp[1]) ^ polyMul(3, tmp[2]) ^ tmp[3];
		state[2][i] = tmp[0] ^ tmp[1] ^ polyMul(2, tmp[2]) ^ polyMul(3, tmp[3]);
		state[3][i] = polyMul(3, tmp[0]) ^ tmp[1] ^ tmp[2] ^ polyMul(2, tmp[3]);
	}

	//MC결과 출력
	cout << "MC: ";
	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j<4; j++)
			printf("%02X ", state[i][j]);
	}
	cout << endl;
}

//inverse MixColumn함수
void invMixColumns()
{
	int tmp[4];
	for (int i = 0; i<4; i++)
	{
		//계산을 위해 tmp에 기존값을 넣는다
		tmp[0] = state[0][i];
		tmp[1] = state[1][i];
		tmp[2] = state[2][i];
		tmp[3] = state[3][i];

		//행렬 계산 수행
		state[0][i] = polyMul(0x0E, tmp[0]) ^ polyMul(0x0B, tmp[1]) ^ polyMul(0x0D, tmp[2]) ^ polyMul(0x09, tmp[3]);
		state[1][i] = polyMul(0x09, tmp[0]) ^ polyMul(0x0E, tmp[1]) ^ polyMul(0x0B, tmp[2]) ^ polyMul(0x0D, tmp[3]);
		state[2][i] = polyMul(0x0D, tmp[0]) ^ polyMul(0x09, tmp[1]) ^ polyMul(0x0E, tmp[2]) ^ polyMul(0x0B, tmp[3]);
		state[3][i] = polyMul(0x0B, tmp[0]) ^ polyMul(0x0D, tmp[1]) ^ polyMul(0x09, tmp[2]) ^ polyMul(0x0E, tmp[3]);

	}
	//MC결과 출력
	cout << "MC: ";
	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j<4; j++)
			printf("%02X ", state[i][j]);
	}
	cout << endl;
}

//암호화 함수
void encryption(int* plain,int* key)
{
	KeyExpansion(key);

	//plain text를 state에 입력한다
	for (int i = 0; i<4; i++)
	{
		for (int j = 0; j<4; j++)
		{
			state[i][j] = plain[i * 4 + j];
		}
	}

	//ROUND 0엔 addRoundKey 수행
	cout << "\nRound 0\n";
	AddroundKey(0);
	cout << endl;

	//ROUND횟수에 맞추어 암호화 수행
	for (int round = 1; round<ROUND; round++)
	{
		cout << "Round " << round << endl;

		//SB SR MC AR을 각각 수행한다
		SubBytes();
		ShiftRows();
		MixColumns();
		AddroundKey(round);
		cout << endl;
	}

	//마지막 라운드에는 SB와 SR AR만 수행한다
	cout << "Round 10\n";
	SubBytes();
	ShiftRows();
	AddroundKey(ROUND);

	//encryption 결과 출력
	cout << "\nCIPHER: ";
	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j<4; j++)
			printf("%02X ", state[i][j]);
	}
	cout << endl<<endl;
}

//복호화 함수
void decryption()
{
	//맨처음엔 addRoundKey 수행
	cout << "Round 0\n";
	AddroundKey(ROUND);
	cout << endl;

	//ROUND횟수에 맞추어 복호화 수행
	for (int round = 1; round<ROUND; round++)
	{
		cout << "Round " << round << endl;

		//inverse SR SB AR MC을 각각 수행한다
		invShiftRows();
		invSubBytes();
		AddroundKey(ROUND - round);
		invMixColumns();
		cout << endl;
	}

	//마지막 라운드에는 SB와 SR AR만 수행한다
	cout << "Round 10\n";
	invShiftRows();
	invSubBytes();
	AddroundKey(0);
	
	//decryption 결과 출력
	cout << "\nDECRYPTED: ";
	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j<4; j++)
			printf("%02X ", state[i][j]);
	}
	cout << endl << endl;
}

//plain text와 key를 입력받고 AES암호화/복호화를 수행한다
int AES(int* plain, int* key)
{
	makeSBOX();
	int RC[10] = { 1 };

	cout << "RC: 01 ";
	for (int i = 1; i < 10; i++)
	{
		RC[i] = polyMul(RC[i - 1], 2);
		printf("%02X ", RC[i]);
	}

	cout << "\nPLAIN: ";
	for(int i=0; i<16; i++)
		printf("%02X ", plain[i]);
	
	cout << "\nKEY: ";
	for (int i = 0; i<16; i++)
		printf("%02X ", key[i]);
	
	cout << "\n\n<------ ENCRYPTION ------->\n\n";
	encryption(plain,key);
	
	cout << "\n\n<------ DECRYPTION ------->\n\n";
	decryption();

	return 0;
}