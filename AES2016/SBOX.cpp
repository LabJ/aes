#include <iostream>
#include <math.h>

using namespace std;

//pol = x8 + x6 + x5 + x2 + 1
//357

#define POLYNOMIAL 357 //ireducible polynomial


//곱연산 함수
int polyMul(int num1, int num2)
{
	//결과값을 저장하는 변수
	int res=0;
	int ip = POLYNOMIAL; //ireducible polynomial
	
	for(int i = 0; i < 8; i++)
	{
		//num2의 비트가 1이면 shift된 num1의 값을 결과에 더한다
		if ((num2 >> i) & 1)
		{
			res ^= num1;
		}
		
		//num1을 shift하고 carry가 발생시 폴리노미얼과 xor
		num1 = num1 << 1;
		
		if (num1 & 256)
		{
			num1 ^= ip;
		}
	}
	return res;
}

//차수를 구하는 함수
int degree(int num)
{
	for (int i = 0; i < 9; i++)
	{
		//carry가 발생하면 차수를 반환한다
		if (num & 256)
		{
			return 8 - i;
		}
		num = num << 1;
	}
	return -1;
}

//나누기 연산
int polyDiv(int bNum,int sNum)
{
	int res=0;
	int d = 0;
	
	while (degree(bNum) >= degree(sNum)) //나누는 값이 크거나 같을동안만 계산하면 된다
	{
		//두 값의 차수의 차이를 구하고 이를 사용해서 나누기 연산을 처리한다
		d = degree(bNum) - degree(sNum);
		bNum = bNum ^(sNum<<d);
		
		res += 1<<d; //몫이 될 값을 더한다
	}
	return res;
}

//GF에서의 inverse연산을 위한 확장된 유클리드 알고리즘
int exEuclid(int num)
{
	//cout << "input number : " << std::hex << num << endl;
	
	//계산을 위한 변수들을 선언한다
	int t = 0;
	int newt = 1;
	int r = POLYNOMIAL;
	int newr = num;

	int q = 0;
	int tmpR = 0;
	int tmpT = 0;

	//메인 알고리즘 영역
	while (newr != 0)
	{
		//다음 계산을 위한 r,t값을 보존한다
		tmpR = newr;
		tmpT = newt;

		//각 값을 구하기위한 연산 수행
		q = polyDiv(r, newr);
		newr = r ^ polyMul(q,newr);
		newt = t ^ polyMul(q,newt);
		
		//mul함수 밖에서 발생하는 carry발생 예외를 처리한다
		if (newr & 256)
			newr ^= POLYNOMIAL;

		//r,t값 update
		r = tmpR;
		t = tmpT;

	}

	//cout << "inverse result : " << t << endl;
	return t;
}

int calculateSBOX(int num)
{
	//GF 테이블 및 계산에 쓰기위한 배열들 생성
	int gfBOX[8][8] = { { 1,0,0,0,1,1,1,1 },{ 1,1,0,0,0,1,1,1 },{ 1,1,1,0,0,0,1,1 },{ 1,1,1,1,0,0,0,1 },{ 1,1,1,1,1,0,0,0 },{ 0,1,1,1,1,1,0,0 },{ 0,0,1,1,1,1,1,0 },{ 0,0,0,1,1,1,1,1 } };
	int MBOX[8] = { 1,1,0,0,0,1,1,0 };
	int input[8];
	int bits[8];

	//inverse값을 저장하는 변수
	int invers = 0;

	invers = exEuclid(num);


	//이 계산을 통해 각 비트b를 나눈다
	for (int b = 0; b < 8; b++)
	{
		input[b] = (invers >> b) & 1; //쉬프트와 & 연산을 통해 구현
	}

	//나눠진 비트들과 테이블의 행렬 계산한다
	for (int i = 0; i < 8; i++)
	{
		bits[i] = 0;

		//행렬 계산 부분
		for (int j = 0; j < 8; j++)
		{
			bits[i] ^= gfBOX[i][j] * input[j]; //인풋 bit와 테이블의 곱
		}
		bits[i] ^= MBOX[i]; //계산된값에 {1,1,0,0,0,1,1,0}을 다시 한번 더한다
	}

	int sNum = 0;

	//이 계산을 통해 계산된 비트 값을 다시 합친다
	for (int s = 0; s < 8; s++)
	{
		//cout << "input " << s << " = " << input[s] << endl;
		//cout << "bit " << s << " = " << bits[s] << endl;
		sNum += (int)pow(2, s)*bits[s];
	}

	//cout << "만들어진 sNum : " << std::hex << sNum << endl;
	
	return sNum;
}

//S-BOX와 inverse S-BOX
int SBOX[256];
int IBOX[256];


//SBOX를 생성한다
void makeSBOX()
{
	
	//S 박스 초기화
	for (int i = 0; i < 256; i++)
	{
			SBOX[i] = calculateSBOX(i);
	}

	//inverse SBOX 초기화 
	for (int i = 0; i < 256; i++)
	{
			IBOX[SBOX[i]] = i;
	}

	//출력으로 테스트
	/*
	for (int i = 0; i < 256; i++)
	{
			printf("%02X ", SBOX[i]);
		if(i%16 == 15)
		cout << endl;
	}
	*/
}


int getSBOX(int num)
{
	return SBOX[num];
}

int getIBOX(int num)
{
	return IBOX[num];
}